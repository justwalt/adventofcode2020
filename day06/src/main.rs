use advent_utils;
use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);
    let vote_groups = advent_utils::create_groups_around_empty_lines(&input_str_vec);

    // Part 1
    let mut unique_sum = 0;
    for group in &vote_groups {
        let mut chosen_letters = HashSet::new();
        for line in group {
            for letter in line.chars() {
                chosen_letters.insert(letter);
            }
        }
        unique_sum += chosen_letters.len()
    }

    println!("Part 1: {}", unique_sum);

    // Part 2
    let mut common_sum = 0;
    for group in &vote_groups {
        let mut chosen_letters: HashMap<char, u64> = HashMap::new();
        for line in group {
            for letter in line.chars() {
                chosen_letters.add(letter);
            }
        }
        // to figure out which letters appeared in every element of the vector,
        // count those that have the same number of instances in the HashMap as
        // there are elements in the group
        for letter in &chosen_letters {
            if letter.1 == &(group.len() as u64) {
                common_sum += 1;
            }
        }
    }

    println!("Part 2: {}", common_sum);
}

// Use this trait to make it easier to either create an entry in a HashMap or, if it
// already exists, increment the count by 1.
pub trait HashMapExt {
    fn add(&mut self, letter: char);
}

impl HashMapExt for HashMap<char, u64> {
    fn add(&mut self, letter: char) {
        let existing_value = match self.get(&letter) {
            Some(l) => l,
            None => &0u64,
        };
        let new_value = existing_value + 1;
        self.insert(letter, new_value);
    }
}
