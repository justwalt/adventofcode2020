use std::fs;
use std::io::{BufRead, BufReader};

pub fn return_file_as_i64_vector(filename: &str) -> Vec<i64> {
    let mut input_vector: Vec<i64> = [].to_vec();

    let file_innards = fs::File::open(filename).expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: i64 = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}

pub fn return_file_as_u32_vector(filename: &str) -> Vec<u32> {
    let mut input_vector: Vec<u32> = [].to_vec();

    let file_innards = fs::File::open(filename).expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: u32 = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}

pub fn return_file_as_u64_vector(filename: &str) -> Vec<u64> {
    let mut input_vector: Vec<u64> = [].to_vec();

    let file_innards = fs::File::open(filename).expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: u64 = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}

pub fn return_file_as_str_vector(filename: &str) -> Vec<String> {
    let mut input_vector: Vec<String> = [].to_vec();

    let file_innards = fs::File::open(filename).expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: String = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    return input_vector;
}

pub fn return_one_line_file_as_string(filename: &str) -> String {
    let mut input_vector: Vec<String> = [].to_vec();

    let file_innards = fs::File::open(filename).expect("Couldn't read the input file!");

    for line in BufReader::new(&file_innards).lines() {
        let line_value: String = line.unwrap().parse().unwrap();
        input_vector.push(line_value);
    }

    assert_eq!(input_vector.len(), 1);

    return (*input_vector[0]).to_string();
}

pub fn create_groups_around_empty_lines(input_str_vec: &Vec<String>) -> Vec<Vec<String>> {
    let mut groups = Vec::new();
    let mut member = Vec::new();

    for line in input_str_vec {
        if line == "" {
            groups.push(member);
            member = Vec::new();
        } else {
            let mut entries = line
                .split(" ")
                .map(|s| s.to_owned())
                .collect::<Vec<String>>();
            member.append(&mut entries);
        }
    }

    if !member.is_empty() {
        // take care of the last empty line in the vector
        groups.push(member);
    }

    groups
}

pub fn break_string_into_n_char_runs(input: &str, n: usize) -> Vec<&str> {
    assert!(input.len() > n);
    // with n=3, breaks "asdfasdf" in "asd", "sdf", "dfa", "fas", "asd", "sdf"
    let mut output: Vec<&str> = Vec::new();
    for i in 0..(input.len() - n) {
        output.push(&input[i..(i + n)]);
    }

    output
}

// // Use this trait to make it easier to either create an entry in a HashMap or, if it
// // already exists, increment the count by 1.
// pub trait HashMapExt {
//     fn add(&mut self, letter: char);
// }

// impl HashMapExt for HashMap<char, u64> {
//     fn add(&mut self, letter: char) {
//         let existing_value = match self.get(&letter) {
//             Some(l) => l,
//             None => &0u64,
//         };
//         let new_value = existing_value + 1;
//         self.insert(letter, new_value);
//     }
// }

// pub trait VecExt {
//     fn is_ascending(&self) -> bool;
// }

// impl VecExt for Vec<u64> {
//     fn is_ascending(&self) -> bool {
//         for i in 1..self.len() {
//             if self[i] < self[i - 1] {
//                 return false;
//             }
//         }

//         true
//     }
// }
