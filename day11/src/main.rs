use advent_utils;
use std::cmp;
use std::collections::HashMap;

/*
    Part 1:
    Basically it's the game of life, but with the addition of walls. Dead cells are "L",
    live cells are "#", and walls are ".". The game ends when the board reaches steady
    state.

    Part 2:
    The rules changes slightly to include not only the spaces directly adjacent to the
    cell in question, but now they look at the first cell that can be "seen" in any
    direction.
*/

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let mut part_one_board = GameBoard::from_input(&input_str_vec);
    part_one_board.run_until_steady_state_p1();

    let mut live_count = 0;
    for (_, cell) in part_one_board.board {
        if cell.state == CellState::Live {
            live_count += 1;
        }
    }

    println!("Part 1: {}", live_count);

    // Part 2
    // let mut part_two_board = GameBoard::from_input(&input_str_vec);

    // println!("Part 2: {}", );
}

#[derive(Debug, Copy, Clone, PartialEq)]
enum CellState {
    Dead,
    Live,
    Wall,
}

#[derive(Debug, Copy, Clone)]
struct Cell {
    state: CellState,
    new_state: Option<CellState>,
    has_changed: bool,
}

impl Cell {
    fn from_char(ch: char) -> Cell {
        let state = match ch {
            'L' => CellState::Dead,
            '#' => CellState::Live,
            '.' => CellState::Wall,
            _ => panic!("Bad character parsed"),
        };

        Cell {
            state,
            new_state: None,
            has_changed: false,
        }
    }

    fn calc_new_state_p1(&mut self, neighbors: &Vec<&Cell>) {
        match self.state {
            CellState::Live => {
                let mut live_neighbors = 0;
                for neigh in neighbors {
                    if neigh.state == CellState::Live {
                        live_neighbors += 1;
                    }
                }
                if live_neighbors >= 4 {
                    self.new_state = Some(CellState::Dead);
                }
            }
            CellState::Dead => {
                for neigh in neighbors {
                    if neigh.state == CellState::Live {
                        return;
                    }
                }
                self.new_state = Some(CellState::Live);
            }
            CellState::Wall => (),
        }
    }

    fn calc_new_state_p2(&mut self, influencers: &Vec<&Cell>) {
        match self.state {
            CellState::Live => {
                let mut live_influencers = 0;
                for neigh in influencers {
                    if neigh.state == CellState::Live {
                        live_influencers += 1;
                    }
                }
                if live_influencers >= 5 {
                    self.new_state = Some(CellState::Dead);
                }
            }
            CellState::Dead => {
                for neigh in influencers {
                    if neigh.state == CellState::Live {
                        return;
                    }
                }
                self.new_state = Some(CellState::Live);
            }
            CellState::Wall => (),
        }
    }

    fn update(&mut self) -> bool {
        // return a bool based on whether or not there was an update
        if self.state == CellState::Wall {
            return false; //
        }

        match self.new_state {
            Some(state) => {
                self.state = state;
                self.has_changed = true;
                self.new_state = None;
                return true;
            }
            None => return false,
        }
    }
}

struct GameBoard {
    board: HashMap<(usize, usize), Cell>,
    iteration: usize,
    dim_x: usize,
    dim_y: usize,
}

impl GameBoard {
    fn from_input(input_str_vec: &Vec<String>) -> GameBoard {
        let mut board = HashMap::new();
        let mut max_x = 0;
        let mut max_y = 0;
        for (y, line) in input_str_vec.iter().enumerate() {
            max_y = if y > max_y { y } else { max_y };
            for (x, ch) in line.chars().enumerate() {
                max_x = if x > max_x { x } else { max_x };
                let new_cell = Cell::from_char(ch);
                board.insert((x, y), new_cell);
            }
        }

        GameBoard {
            board,
            iteration: 0,
            dim_x: max_x,
            dim_y: max_y,
        }
    }

    fn run_until_steady_state_p1(&mut self) {
        while self.perform_iteration_p1() {
            ();
        }
    }

    fn run_until_steady_state_p2(&mut self) {
        while self.perform_iteration_p2() {
            ();
        }
    }

    fn perform_iteration_p1(&mut self) -> bool {
        let mut change_occurred = false;

        let board_clone = self.board.clone();
        for (coords, _) in &board_clone {
            let lo_x = coords.0.saturating_sub(1);
            let hi_x = coords.0 + 1;
            let lo_y = coords.1.saturating_sub(1);
            let hi_y = coords.1 + 1;

            let mut neighbors = Vec::new();
            for y in lo_y..=hi_y {
                for x in lo_x..=hi_x {
                    if coords == &(x, y) {
                        continue;
                    }
                    match board_clone.get(&(x, y)) {
                        Some(c) => neighbors.push(c),
                        None => (),
                    }
                }
            }

            let cell = self.board.get_mut(&coords).unwrap();
            cell.calc_new_state_p1(&neighbors);
        }

        for (_, cell) in &mut self.board {
            if cell.update() {
                change_occurred = true;
            }
        }

        self.iteration += 1;
        change_occurred
    }

    fn perform_iteration_p2(&mut self) -> bool {
        let mut change_occurred = false;

        let board_clone = self.board.clone();
        for (coords, _) in &board_clone {
            //

            let mut influencers = Vec::new();
            //

            let cell = self.board.get_mut(&coords).unwrap();
            cell.calc_new_state_p2(&influencers);
        }

        for (_, cell) in &mut self.board {
            if cell.update() {
                change_occurred = true;
            }
        }

        self.iteration += 1;
        change_occurred
    }

    fn print_board(&self) {
        println!("Iteration {}:", self.iteration);
        for y in 0..=self.dim_y {
            print!("    ");
            for x in 0..=self.dim_x {
                let char_to_print = match self.board[&(x, y)].state {
                    CellState::Dead => 'L',
                    CellState::Live => '#',
                    CellState::Wall => '.',
                };
                print!("{}", char_to_print);
            }
            println!();
        }
    }
}
