def main():
    with open("./input.txt", "r") as f:
        data = f.readlines()

    binaries = []
    for line in data:
        binaries.append(
            int(
                line.replace("F", "0").replace("B", "1").replace("R", "1").replace("L", "0"), 2
            )
        )

    print("Part 1: {}".format(max(binaries)))

    sorted_bin = sorted(binaries)

    counter = sorted_bin[0]
    for binary in sorted_bin:
        if counter != binary:
            print("Part 2: {}".format(counter))
            break
        counter += 1


if __name__ == "__main__":
    main()
