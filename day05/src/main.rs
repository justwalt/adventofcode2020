use advent_utils;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let binary_vec = parse_to_binary(&input_str_vec);
    let binu64_vec = parse_binary_to_u64(&binary_vec);

    let max_u64 = binu64_vec.iter().max().unwrap();
    println!("Part 1: {}", max_u64);

    // Part 2
    let gap_number = find_gap(&binu64_vec);
    println!("Part 2: {}", gap_number);
}

fn parse_to_binary(input_str_vec: &Vec<String>) -> Vec<String> {
    let mut binary_vec: Vec<String> = Vec::new();
    for instr in input_str_vec {
        let mut binary_str: String = "".to_string();
        for ch in instr.chars() {
            binary_str.push_str(match ch {
                'F' => "0",
                'B' => "1",
                'R' => "1",
                'L' => "0",
                _ => panic!(),
            });
        }

        binary_vec.push(binary_str);
    }

    binary_vec
}

fn parse_binary_to_u64(binary_vec: &Vec<String>) -> Vec<u64> {
    let mut binu64_vec: Vec<u64> = Vec::new();
    for binstr in binary_vec {
        let binu64 = u64::from_str_radix(binstr, 2).unwrap();
        binu64_vec.push(binu64);
    }

    binu64_vec
}

fn find_gap(binu64_vec: &Vec<u64>) -> u64 {
    let mut sorted_binu64_vec = binu64_vec.clone();
    sorted_binu64_vec.sort();

    let mut counter = sorted_binu64_vec.iter().min().unwrap().clone();
    for binval in sorted_binu64_vec {
        if counter != binval {
            return counter;
        }
        counter += 1;
    }

    return 0;
}
