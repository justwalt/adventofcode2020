use advent_utils;
use regex::Regex;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let passports = advent_utils::create_groups_around_empty_lines(&input_str_vec);
    let valid_passports = count_valid_passports_p1(&passports);

    println!("Part 1: {}", valid_passports);

    // Part 2
    let valid_passports = count_valid_passports_p2(&passports);

    println!("Part 2: {}", valid_passports);
}

fn count_valid_passports_p1(passports: &Vec<Vec<String>>) -> u64 {
    let neccessary_creds: [&str; 7] = ["byr", "iyr", "eyr", "hgt", "hcl", "ecl", "pid"]; // ignore cid
    let mut valid_count = 0;

    for port in passports {
        let mut cred_count = 0;
        for cred in port {
            if neccessary_creds.contains(&&cred[0..3]) {
                cred_count += 1;
            }
        }
        if cred_count >= 7 {
            valid_count += 1;
        } else {
            ()
            // dbg!(&port);
        }
    }

    valid_count
}

fn count_valid_passports_p2(passports: &Vec<Vec<String>>) -> u64 {
    let mut valid_count = 0;

    for port in passports {
        let mut cred_count = 0;
        for cred in port {
            if verify_cred(cred) {
                cred_count += 1;
            // println!("{}: true", &cred);
            } else {
                // println!("{}: false", &cred);
            }
        }
        if cred_count >= 7 {
            valid_count += 1;
        }
    }

    valid_count
}

fn verify_cred(cred: &str) -> bool {
    let prefix = &cred[0..3];
    let suffix = &cred[4..];
    match prefix {
        "byr" => verify_byr(suffix),
        "iyr" => verify_iyr(suffix),
        "eyr" => verify_eyr(suffix),
        "hgt" => verify_hgt(suffix),
        "hcl" => verify_hcl(suffix),
        "ecl" => verify_ecl(suffix),
        "pid" => verify_pid(suffix),
        "cid" => false,
        _ => panic!(),
    }
}

fn verify_byr(byr: &str) -> bool {
    // 1920-2002
    let year = match byr.parse::<u32>() {
        Ok(year) => year,
        Err(_) => return false,
    };

    if (1920..=2002).contains(&year) {
        return true;
    } else {
        return false;
    }
}

fn verify_iyr(iyr: &str) -> bool {
    // 2010-2020
    let year = match iyr.parse::<u32>() {
        Ok(year) => year,
        Err(_) => return false,
    };

    if (2010..=2020).contains(&year) {
        return true;
    } else {
        return false;
    }
}

fn verify_eyr(eyr: &str) -> bool {
    // 2020-2030
    let year = match eyr.parse::<u32>() {
        Ok(year) => year,
        Err(_) => return false,
    };

    if (2020..=2030).contains(&year) {
        return true;
    } else {
        return false;
    }
}

fn verify_hgt(hgt: &str) -> bool {
    // 150-193cm, or 59-76in
    let re = Regex::new(r"(\d+)([a-z]{2})").unwrap();
    let caps = match re.captures(hgt) {
        Some(cap) => cap,
        None => return false,
    };

    let value_str = match caps.get(1) {
        Some(val) => val.as_str(),
        None => return false,
    };
    let value = match value_str.parse::<u32>() {
        Ok(valu64) => valu64,
        Err(_) => return false,
    };
    let units = match caps.get(2) {
        Some(unt) => unt.as_str(),
        None => return false,
    };

    match units {
        "in" => {
            if (59..=76).contains(&value) {
                return true;
            } else {
                return false;
            }
        }
        "cm" => {
            if (150..=193).contains(&value) {
                return true;
            } else {
                return false;
            }
        }
        _ => return false,
    }
}

fn verify_hcl(hcl: &str) -> bool {
    // #ffffff (# and 6 hex digits)
    let re = Regex::new(r"#[a-f0-9]{6}").unwrap();
    match re.captures(hcl) {
        Some(_) => return true,
        None => return false,
    };
}

fn verify_ecl(ecl: &str) -> bool {
    let options = ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"];
    if options.contains(&ecl) {
        true
    } else {
        false
    }
}

fn verify_pid(pid: &str) -> bool {
    // nine digit number including leading 0s
    if pid.len() != 9 {
        return false;
    }

    match pid.parse::<u32>() {
        Ok(_) => return true,
        Err(_) => return false,
    }
}
