use advent_utils;

/*
   Part 1:
   Find the two entries that sum to 2020 and then multiply those two numbers together.

   Part 2:
   Find the product of the three entries that sum to 2020.
*/

fn main() {
    let input_file = "./input.txt";
    let input_u64_vec = advent_utils::return_file_as_u64_vector(&input_file);

    // Part 1
    let mut result: u64 = 0;
    'finder1: for i in &input_u64_vec {
        for j in &input_u64_vec {
            if i + j == 2020 {
                result = i * j;
                break 'finder1;
            }
        }
    }

    println!("Part 1: {}", result);

    // Part 2
    let mut result: u64 = 0;
    'finder2: for i in &input_u64_vec {
        for j in &input_u64_vec {
            for k in &input_u64_vec {
                if i + j + k == 2020 {
                    result = i * j * k;
                    break 'finder2;
                }
            }
        }
    }

    println!("Part 2: {}", result);
}
