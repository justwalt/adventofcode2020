#!/bin/bash

for i in $(seq 1 9); do
    dirname="day0$i"
    cargo new --vcs=none "$dirname"
    touch "$dirname""/input.txt"
    echo 'advent-utils = { path = "../advent-utils" }' >> "$dirname""/Cargo.toml"
    cp ./father_main.rs "$dirname""/src/main.rs"
done

for i in $(seq 10 25); do
    dirname="day$i"
    cargo new --vcs=none "$dirname"
    touch "$dirname""/input.txt"
    echo 'advent-utils = { path = "../advent-utils" }' >> "$dirname""/Cargo.toml"
    cp ./father_main.rs "$dirname""/src/main.rs"
done
