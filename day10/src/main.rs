use advent_utils;
use std::collections::HashMap;

fn main() {
    let input_file = "./input.txt";
    let mut input_u64_vec = advent_utils::return_file_as_u64_vector(&input_file);

    // Part 1
    input_u64_vec.push(0);
    input_u64_vec.push(input_u64_vec.iter().max().unwrap() + 3);
    input_u64_vec.sort();
    let differences = find_differences(&input_u64_vec);
    let product = differences[&3] * differences[&1];

    println!("Part 1: {}", product);

    // Part 2
    let mut tree = FakeTree::new(input_u64_vec);
    let sum_val = tree.sum_leaves();
    println!("Part 2: {}", sum_val);
}

fn find_differences(sorted_u64s: &Vec<u64>) -> HashMap<u64, u64> {
    let mut diffs = HashMap::new();
    for i in (0..sorted_u64s.len()).skip(1) {
        let diff = sorted_u64s[i] - sorted_u64s[i - 1];
        diffs.add(diff);
    }

    diffs
}

#[derive(Clone, Debug)]
struct FakeTree {
    leaves: HashMap<u64, Leaf>,
    input_vals: Vec<u64>,
}

impl FakeTree {
    fn new(sorted_u64s: Vec<u64>) -> FakeTree {
        let mut leaves = HashMap::new();
        leaves.insert(0, Leaf::new(0, &[]));
        leaves.insert(
            sorted_u64s[1],
            Leaf::new(sorted_u64s[1], &sorted_u64s[0..1]),
        );
        leaves.insert(
            sorted_u64s[2],
            Leaf::new(sorted_u64s[2], &sorted_u64s[0..2]),
        );

        for i in 3..sorted_u64s.len() {
            leaves.insert(
                sorted_u64s[i],
                Leaf::new(sorted_u64s[i], &sorted_u64s[(i - 3)..i]),
            );
        }

        FakeTree {
            leaves,
            input_vals: sorted_u64s,
        }
    }

    fn sum_leaves(&mut self) -> u64 {
        for value in &self.input_vals {
            let mut sum = 0;
            let leaf_choices = self.leaves[&value].choices.clone();
            for key in leaf_choices {
                sum += self.leaves[&key].value.unwrap();
            }
            if sum == 0 {
                sum = 1;
            }
            let mut leaf = self.leaves.get_mut(&value).unwrap();
            leaf.value = Some(sum);
            // println!("value: {} {:?} -> {}", value, leaf.choices, sum);
        }

        let max_key = self.leaves.keys().max().unwrap();
        self.leaves[max_key].value.unwrap()
    }
}

#[derive(Clone, Debug)]
struct Leaf {
    choices: Vec<u64>,
    value: Option<u64>,
}

impl Leaf {
    fn new(seed: u64, preceding: &[u64]) -> Leaf {
        let mut choices = Vec::new();
        for val in preceding {
            if val + 3 >= seed {
                choices.push(*val);
            }
        }

        let value = if choices.is_empty() { Some(1) } else { None };

        Leaf { choices, value }
    }
}

// Use this trait to make it easier to either create an entry in a HashMap or, if it
// already exists, increment the count by 1.
pub trait HashMapExt {
    fn add(&mut self, value: u64);
}

impl HashMapExt for HashMap<u64, u64> {
    fn add(&mut self, value: u64) {
        let existing_value = match self.get(&value) {
            Some(l) => l,
            None => &0u64,
        };
        let new_value = existing_value + 1;
        self.insert(value, new_value);
    }
}
