use advent_utils;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    let hill = Hill::new(input_str_vec);

    // // Part 1
    let tree_count = hill.check_slope(1, 3);
    println!("Part 1: {}", &tree_count);

    // // Part 2
    let tree_count_1_1 = hill.check_slope(1, 1);
    let tree_count_1_3 = hill.check_slope(1, 3);
    let tree_count_1_5 = hill.check_slope(1, 5);
    let tree_count_1_7 = hill.check_slope(1, 7);
    let tree_count_2_1 = hill.check_slope(2, 1);

    let tree_product =
        tree_count_1_1 * tree_count_1_3 * tree_count_1_5 * tree_count_1_7 * tree_count_2_1;

    println!("Part 2: {}", tree_product);
}

struct Hill {
    trees: Vec<String>,
    width: usize,
    length: usize,
}

impl Hill {
    fn new(input_str_vec: Vec<String>) -> Hill {
        let width = *(&input_str_vec[0].len().clone());
        let length = *(&input_str_vec.len().clone());
        Hill {
            trees: input_str_vec,
            width,
            length,
        }
    }

    fn check_tree(&self, x: usize, y: usize) -> bool {
        let new_x = x % self.width;
        match self.trees[y].chars().collect::<Vec<char>>()[new_x] {
            '#' => true,
            '.' => false,
            _ => panic!(),
        }
    }

    fn check_slope(&self, rise: usize, run: usize) -> u64 {
        let mut row_index = 0;
        let mut col_index = 0;

        let mut tree_count = 0;

        while row_index < self.length {
            tree_count += match self.check_tree(col_index, row_index) {
                true => 1,
                false => 0,
            };

            row_index += rise;
            col_index += run;
        }

        tree_count
    }
}
