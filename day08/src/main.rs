use advent_utils;
use std::collections::HashSet;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let mut comp = Computer::new(&input_str_vec);
    let accval_upon_repeat = comp.run_program().0;
    println!("Part 1: {}", accval_upon_repeat);

    // Part 2
    for i in 0..input_str_vec.len() {
        comp.reset();
        if comp.fix_program(i) {
            let return_val = comp.run_program();
            if return_val.1 {
                println!("Part 2: {}", return_val.0);
                break;
            }
            comp.fix_program(i);
        };
    }
}

struct Computer {
    instructions: Vec<Operation>,
    exec_index: usize,
    accumulator: i64,
}

impl Computer {
    fn new(raw_ops: &Vec<String>) -> Computer {
        let mut instructions = Vec::new();
        for raw_op in raw_ops {
            let parsed_op = Operation::parse_from_str(raw_op);
            instructions.push(parsed_op);
        }
        let exec_index = 0;
        let accumulator = 0;

        Computer {
            instructions,
            exec_index,
            accumulator,
        }
    }

    fn reset(&mut self) {
        self.exec_index = 0;
        self.accumulator = 0;
    }

    fn run_program(&mut self) -> (i64, bool) {
        let mut indices_of_performed = HashSet::new();
        loop {
            if indices_of_performed.contains(&self.exec_index) {
                return (self.accumulator, false);
            }
            if self.exec_index > self.instructions.len() {
                return (self.accumulator, false);
            }
            if self.exec_index == self.instructions.len() {
                return (self.accumulator, true);
            }

            // println!(
            //     "{} ({}): {:?}",
            //     self.exec_index, self.accumulator, self.instructions[self.exec_index]
            // );

            indices_of_performed.insert(self.exec_index);

            // decide what to do based on the operation to be executed
            let op = &self.instructions[self.exec_index];

            match op.instr {
                Instruction::Nop => self.exec_index += 1,
                Instruction::Acc => {
                    self.exec_index += 1;
                    self.accumulator += op.value;
                }
                Instruction::Jmp => self.exec_index += op.value as usize,
            }
        }
    }

    fn fix_program(&mut self, index: usize) -> bool {
        match self.instructions[index].instr {
            Instruction::Jmp => self.instructions[index].instr = Instruction::Nop,
            Instruction::Nop => self.instructions[index].instr = Instruction::Jmp,
            _ => return false,
        }

        return true;
    }
}

#[derive(Debug)]
struct Operation {
    instr: Instruction,
    value: i64,
}

impl Operation {
    fn parse_from_str(raw_op: &str) -> Operation {
        let split_str = raw_op
            .split(' ')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        let instr = match split_str[0].as_str() {
            "nop" => Instruction::Nop,
            "acc" => Instruction::Acc,
            "jmp" => Instruction::Jmp,
            _ => panic!("Bad instruction value"),
        };
        let value = split_str[1].parse::<i64>().unwrap();

        Operation { instr, value }
    }
}

#[derive(Debug)]
enum Instruction {
    Nop,
    Acc,
    Jmp,
}
