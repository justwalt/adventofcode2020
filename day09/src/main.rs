use advent_utils;

fn main() {
    let input_file = "./input.txt";
    let input_u64_vec = advent_utils::return_file_as_u64_vector(&input_file);

    // Part 1
    let first_invalid = get_first_invalid_number(&input_u64_vec, 25);
    println!("Part 1: {}", first_invalid);

    // Part 2
    let result_indices = find_run_which_sums_to_x(first_invalid, &input_u64_vec).unwrap();
    let result_range = &input_u64_vec[result_indices.0..result_indices.1];
    let result_sum = result_range.iter().min().unwrap() + result_range.iter().max().unwrap();
    println!("Part 2: {}", result_sum);
}

fn get_first_invalid_number(input_u64_vec: &Vec<u64>, check_before: usize) -> u64 {
    let mut lo = 0;
    for hi in check_before..input_u64_vec.len() {
        let x = input_u64_vec[hi];
        let values = &input_u64_vec[lo..hi];

        if !can_x_be_the_sum_of_two_members(x, values) {
            return input_u64_vec[hi];
        }

        lo += 1;
    }

    return 0;
}

fn can_x_be_the_sum_of_two_members(x: u64, values: &[u64]) -> bool {
    for i in 0..values.len() {
        for j in (i + 1)..values.len() {
            if values[i] + values[j] == x {
                return true;
            }
        }
    }

    return false;
}

fn find_run_which_sums_to_x(x: u64, input_u64_vec: &Vec<u64>) -> Option<(usize, usize)> {
    for i in 2.. {
        for j in i..input_u64_vec.len() {
            if input_u64_vec[i..j].iter().sum::<u64>() == x {
                return Some((i, j));
            }
        }
    }

    return None;
}
