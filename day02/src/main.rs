use advent_utils;

/*
    Part 1:
    Each line gives the password policy and then the password. The password policy
    indicates the lowest and highest number of times a given letter must appear for the
    password to be valid.

    Part 2:
    Each policy actually describes two positions in the password, where 1 means the
    first character, 2 means the second character, and so on. Exactly one of these
    positions must contain the given letter (they are 1-indexed).
*/

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let mut valid_total = 0;
    for input_str in &input_str_vec {
        if Password::new(input_str).is_valid_part_one() {
            valid_total += 1;
        };
    }

    println!("Part 1: {}", valid_total);

    // Part 2
    let mut valid_total = 0;
    for input_str in &input_str_vec {
        if Password::new(input_str).is_valid_part_two() {
            valid_total += 1;
        };
    }

    println!("Part 2: {}", valid_total);
}

struct Password {
    lo: usize,
    hi: usize,
    letter: char,
    pass_str: String,
}

impl Password {
    fn new(input_str: &str) -> Password {
        let split_input = input_str
            .split(' ')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();

        let split_range = &split_input[0]
            .split('-')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();

        let lo = split_range[0].parse::<usize>().unwrap();
        let hi = split_range[1].parse::<usize>().unwrap();

        let letter = split_input[1].chars().collect::<Vec<char>>()[0];

        let pass_str = split_input[2].clone();

        Password {
            lo,
            hi,
            letter,
            pass_str,
        }
    }

    fn is_valid_part_one(&self) -> bool {
        if (self.lo..=self.hi).contains(
            &self
                .pass_str
                .match_indices(self.letter)
                .collect::<Vec<(usize, &str)>>()
                .len(),
        ) {
            return true;
        } else {
            return false;
        }
    }

    fn is_valid_part_two(&self) -> bool {
        let pos_one = self.lo - 1;
        let pos_two = self.hi - 1;

        let mut cond_count = 0;
        if self.pass_str.chars().collect::<Vec<char>>()[pos_one] == self.letter {
            cond_count += 1;
        }
        if self.pass_str.chars().collect::<Vec<char>>()[pos_two] == self.letter {
            cond_count += 1;
        }

        cond_count == 1
    }
}
