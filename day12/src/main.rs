use advent_utils;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    let mut ship = Ship::new();
    for line in &input_str_vec {
        let action = Action::from_str(&line);
        ship.perform_action(&action);
    }

    let dist = ship.distance_from_origin();
    println!("Part 1: {}", dist);

    // Part 2
    let mut wayship = WaypointShip::new();
    for line in &input_str_vec {
        let action = Action::from_str(&line);
        wayship.perform_action(&action);
    }

    let waydist = wayship.distance_from_origin();
    println!("Part 2: {}", waydist);
}

// Part 1
struct Ship {
    x_pos: i64,
    y_pos: i64,
    angle: i64,
}

impl Ship {
    fn new() -> Ship {
        let x_pos = 0;
        let y_pos = 0;
        let angle = 0;

        Ship {
            x_pos,
            y_pos,
            angle,
        }
    }

    fn perform_action(&mut self, action: &Action) {
        match action.movement {
            Direction::North => {
                self.x_pos += action.amount;
            }
            Direction::South => {
                self.x_pos -= action.amount;
            }
            Direction::East => {
                self.y_pos -= action.amount;
            }
            Direction::West => {
                self.y_pos += action.amount;
            }
            Direction::Left => {
                self.turn(action.amount);
            }
            Direction::Right => {
                self.turn(360 - action.amount);
            }
            Direction::Forward => {
                let direction = match self.angle {
                    0 => Direction::East,
                    90 => Direction::North,
                    180 => Direction::West,
                    270 => Direction::South,
                    _ => panic!("Bad angle"),
                };
                let new_action = Action::from_manual(direction, action.amount);
                self.perform_action(&new_action);
            }
        }
    }

    fn turn(&mut self, degrees: i64) {
        self.angle = (self.angle + degrees) % 360;
    }

    fn distance_from_origin(&self) -> u64 {
        self.x_pos.abs() as u64 + self.y_pos.abs() as u64
    }
}

struct Action {
    movement: Direction,
    amount: i64,
}

impl Action {
    fn from_str(action: &str) -> Action {
        let movement = match action.chars().next().unwrap() {
            'N' => Direction::North,
            'S' => Direction::South,
            'E' => Direction::East,
            'W' => Direction::West,
            'L' => Direction::Left,
            'R' => Direction::Right,
            'F' => Direction::Forward,
            _ => panic!("Bad movement parse!"),
        };

        let amount = action[1..].parse::<i64>().unwrap();

        Action { movement, amount }
    }

    fn from_manual(movement: Direction, amount: i64) -> Action {
        Action { movement, amount }
    }
}

// Part 2
struct WaypointShip {
    ship_x: i64,
    ship_y: i64,
    way_dx: i64,
    way_dy: i64,
}

impl WaypointShip {
    fn new() -> WaypointShip {
        let ship_x = 0;
        let ship_y = 0;
        let way_dx = 10;
        let way_dy = 1;

        WaypointShip {
            ship_x,
            ship_y,
            way_dx,
            way_dy,
        }
    }

    fn perform_action(&mut self, action: &Action) {
        match action.movement {
            Direction::North => {
                self.way_dy += action.amount;
            }
            Direction::South => {
                self.way_dy -= action.amount;
            }
            Direction::East => {
                self.way_dx += action.amount;
            }
            Direction::West => {
                self.way_dx -= action.amount;
            }
            Direction::Left => {
                self.rotate(action.amount);
            }
            Direction::Right => {
                self.rotate(360 - action.amount);
            }
            Direction::Forward => {
                self.ship_x += action.amount * self.way_dx;
                self.ship_y += action.amount * self.way_dy;
            }
        }
    }

    fn rotate(&mut self, degrees: i64) {
        match degrees {
            90 => {
                let new_dx = -self.way_dy;
                let new_dy = self.way_dx;
                self.way_dx = new_dx;
                self.way_dy = new_dy;
            }
            180 => {
                let new_dx = -self.way_dx;
                let new_dy = -self.way_dy;
                self.way_dx = new_dx;
                self.way_dy = new_dy;
            }
            270 => {
                let new_dx = self.way_dy;
                let new_dy = -self.way_dx;
                self.way_dx = new_dx;
                self.way_dy = new_dy;
            }
            _ => panic!("Bad angle (waypoint ship)"),
        }
    }

    fn distance_from_origin(&self) -> u64 {
        self.ship_x.abs() as u64 + self.ship_y.abs() as u64
    }
}

// Shared
#[derive(Copy, Clone, PartialEq)]
enum Direction {
    North,
    South,
    East,
    West,
    Left,
    Right,
    Forward,
}
