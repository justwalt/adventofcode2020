fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    let ideal_departure_time = input_str_vec[0].parse::<u64>().unwrap();
    let bus_ids = parse_buses_p1(&input_str_vec[1]);

    // Part 1
    let mut earliest_bus = EarliestBus::new(ideal_departure_time);

    for id in &bus_ids {
        earliest_bus.check_earlier(*id);
    }

    let part_one = earliest_bus.calc_answer();
    println!("Part 1: {}", part_one);

    // Part 2 tests
    let test1_str = "17,x,13,19";
    let (t1_bus_ids, t1_spacings) = parse_buses_p2(&test1_str);
    let test1 = find_correctly_spaced_sequence(&t1_bus_ids, &t1_spacings);
    println!("Test 1: {} ({})", test1, test1 == 3417);
    let test2_str = "67,7,59,61";
    let (t2_bus_ids, t2_spacings) = parse_buses_p2(&test2_str);
    let test2 = find_correctly_spaced_sequence(&t2_bus_ids, &t2_spacings);
    println!("Test 2: {} ({})", test2, test2 == 754018);
    let test3_str = "67,x,7,59,61";
    let (t3_bus_ids, t3_spacings) = parse_buses_p2(&test3_str);
    let test3 = find_correctly_spaced_sequence(&t3_bus_ids, &t3_spacings);
    println!("Test 3: {} ({})", test3, test3 == 779210);
    let test4_str = "67,7,x,59,61";
    let (t4_bus_ids, t4_spacings) = parse_buses_p2(&test4_str);
    let test4 = find_correctly_spaced_sequence(&t4_bus_ids, &t4_spacings);
    println!("Test 4: {} ({})", test4, test4 == 1261476);
    let test5_str = "1789,37,47,1889";
    let (t5_bus_ids, t5_spacings) = parse_buses_p2(&test5_str);
    let test5 = find_correctly_spaced_sequence(&t5_bus_ids, &t5_spacings);
    println!("Test 5: {} ({})", test5, test5 == 1202161486);

    // Part 2
    let (bus_ids, spacings) = parse_buses_p2(&input_str_vec[1]);
    let part_two = find_correctly_spaced_sequence(&bus_ids, &spacings);
    println!("Part 2: {}", part_two);
}

struct EarliestBus {
    departure_time: u64,
    earliest_id: u64,
    earliest_diff: u64,
}

impl EarliestBus {
    fn new(departure_time: u64) -> EarliestBus {
        let earliest_id = 0;
        let earliest_diff = u64::MAX;

        EarliestBus {
            departure_time,
            earliest_id,
            earliest_diff,
        }
    }

    fn check_earlier(&mut self, id: u64) {
        let new_diff =
            find_first_multiple_above_value(id, self.departure_time) - self.departure_time;
        if new_diff < self.earliest_diff {
            self.earliest_id = id;
            self.earliest_diff = new_diff;
        }
    }

    fn calc_answer(&self) -> u64 {
        self.earliest_id * self.earliest_diff
    }
}

fn parse_buses_p1(schedule: &str) -> Vec<u64> {
    let split_buses = schedule.split(',').collect::<Vec<&str>>();
    let mut schedule_u64s = Vec::new();

    for bus in split_buses {
        if let Ok(b) = bus.parse::<u64>() {
            schedule_u64s.push(b)
        }
    }

    schedule_u64s
}

fn find_first_multiple_above_value(factor: u64, value: u64) -> u64 {
    // assert!(factor < value);
    if factor > value {
        println!("{}, {}", factor, value);
        panic!("Oh no!");
    }
    let mut result = (value / factor) * factor;
    if result < value {
        result += factor;
    }

    result
}

fn parse_buses_p2(schedule: &str) -> (Vec<u64>, Vec<u64>) {
    // [7, 13, x, x, 59, x, 31, 19] -> [1, 3, 2, 1]
    let split_buses = schedule.split(',').collect::<Vec<&str>>();
    let mut bus_ids = Vec::new();
    let mut differences = Vec::new();
    let mut diff = 1;

    for bus in split_buses {
        match bus.parse::<u64>() {
            Ok(id) => {
                bus_ids.push(id);
                differences.push(diff);
                diff = 1;
            }
            Err(_) => diff += 1,
        }
    }

    (bus_ids, differences[1..].to_vec())
}

fn find_correctly_spaced_sequence(bus_ids: &[u64], differences: &[u64]) -> u64 {
    let max_id = *bus_ids.iter().max().unwrap();
    let skip_factor = max_id * bus_ids[0];
    let skip_offset = find_first_potential_solution(&bus_ids, &differences);
}

fn find_first_potential_solution(bus_ids: &[u64], differences: &[u64]) -> u64 {}

fn x_find_correctly_spaced_sequence(bus_ids: &[u64], differences: &[u64]) -> u64 {
    let max_id = *bus_ids.iter().max().unwrap();
    // let skip_factor = get_skip_factor(&bus_ids, &differences);
    let mut value = find_first_multiple_above_value(bus_ids[0], max_id); // + skip_factor;

    loop {
        value += bus_ids[0];
        let mut first_mults = Vec::new();

        for id in bus_ids {
            let mult = find_first_multiple_above_value(*id, value);
            first_mults.push(mult);
        }

        if verify_differences(&first_mults, &differences) {
            return first_mults[0];
        }
    }
}

fn verify_differences(vector: &[u64], differences: &[u64]) -> bool {
    if vector.len() != differences.len() + 1 {
        return false;
    }

    for i in 0..differences.len() {
        if vector[i + 1] - vector[i] != differences[i] {
            return false;
        }
    }

    true
}

fn get_skip_factor(bus_ids: &[u64], differences: &[u64]) -> u64 {
    // [7, 13, 59, 31, 19]
    //   [1,  3,  2,  1]
    // take index of max, sum the values in differences up until that point
    let maxval = bus_ids.iter().max().unwrap();
    let maxidx = bus_ids.iter().position(|&r| r == *maxval).unwrap();

    differences[0..maxidx].iter().sum()
}
