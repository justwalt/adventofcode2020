use advent_utils;
use std::collections::HashMap;
use std::collections::HashSet;

fn main() {
    let input_file = "./input.txt";
    let input_str_vec = advent_utils::return_file_as_str_vector(&input_file);

    // Part 1
    println!("{:?}", parse_bag_lines(&input_str_vec));

    // println!("Part 1: {}", );

    // Part 2

    // println!("Part 2: {}", );
}

fn create_bag_map(bag_input_vec: &Vec<Vec<String>>) -> HashMap<&str, bool> {}

fn parse_bags(input_str_vec: &Vec<String>) -> Vec<Vec<String>> {
    let mut bags = Vec::new();
    for line in input_str_vec {
        let split_string = line
            .split(' ')
            .map(|s| s.to_string())
            .collect::<Vec<String>>();
        bags.push(split_string);
    }

    bags
}

fn bags_within(bag_line: &Vec<String>) -> Vec<String> {
    let mut inner_bags = Vec::new();
    match bag_line.len() {
        7 => (),
        8 => {
            let bag_adj = bag_line[5];
            let bag_col = bag_line[6];
            inner_bags.push([bag_adj, bag_col].join(" "));
        }
        12 => {
            let bag_adj = bag_line[5];
            let bag_col = bag_line[6];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[9];
            let bag_col = bag_line[10];
            inner_bags.push([bag_adj, bag_col].join(" "));
        }
        16 => {
            let bag_adj = bag_line[5];
            let bag_col = bag_line[6];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[9];
            let bag_col = bag_line[10];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[13];
            let bag_col = bag_line[14];
            inner_bags.push([bag_adj, bag_col].join(" "));
        }
        20 => {
            let bag_adj = bag_line[5];
            let bag_col = bag_line[6];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[9];
            let bag_col = bag_line[10];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[13];
            let bag_col = bag_line[14];
            inner_bags.push([bag_adj, bag_col].join(" "));
            let bag_adj = bag_line[17];
            let bag_col = bag_line[18];
            inner_bags.push([bag_adj, bag_col].join(" "));
        }
        _ => panic!("Bad line length"),
    };

    inner_bags
}

struct Bag {
    name: String,
    contains: Vec<String>,
    has_gold: bool,
}

impl Bag {
    fn new(name: String, contains: Vec<String>, has_gold: bool) -> Bag {
        Bag {
            name,
            contains,
            has_gold,
        }
    }

    //
}

// scraps

fn parse_bag_lines(input_str_vec: &Vec<String>) -> HashSet<usize> {
    let mut lengths = HashSet::new();

    for line in input_str_vec {
        lengths.insert(parse_line(&line));
    }

    lengths
}

fn parse_line(line: &String) -> usize {
    // split func was here
    match split_string.len() {
        7 => (),
        8 => (),
        12 => (),
        16 => (),
        20 => (),
        _ => panic!("Bad line length"),
    };

    // if split_string.len() == 7 {
    //     // no other bags
    //     println!(
    //         "{} {}, {}",
    //         split_string[0], split_string[1], split_string[4]
    //     );
    // }
}

// 20: dull lavender bags contain 5 clear fuchsia bags, 1 muted blue bag, 1 shiny gold bag, 5 dotted turquoise bags.
//  8: drab orange bags contain 1 dull lavender bag.
//  7: dim tan bags contain no other bags.
